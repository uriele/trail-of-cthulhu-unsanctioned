import { abilityCategories, combatAbilities, seenPopup } from "../constants";
import system from "../system.json";
import { UpgradeNote } from "./UpgradeNote";

export const registerSettings = function () {
  game.settings.register(system.name, abilityCategories, {
    name: "Ability categories",
    hint: "Comma-separated",
    scope: "world",
    config: true,
    default: "Academic,Interpersonal,Technical",
    type: String,
  });
  game.settings.register(system.name, combatAbilities, {
    name: "Combat abilities",
    hint: "Comma-separated",
    scope: "world",
    config: true,
    default: "Scuffling,Weapons,Firearms,Athletics",
    type: String,
  });
  game.settings.register(system.name, seenPopup, {
    name: "Hide GUMSHOE note",
    hint: "Don't show the note about the new GUMSHOE system every time you log in.",
    scope: "client",
    config: false,
    default: false,
    type: Boolean,
  });
  // Define a settings submenu which handles advanced configuration needs
  game.settings.registerMenu(system.name, "upgradeNote", {
    name: "Upgrading",
    label: "About the new GUMSHOE System for FVTT", // The text label used in the button
    // hint: "A description of what will occur in the submenu dialog.",
    icon: "fas fa-info-circle", // A Font Awesome icon used in the submenu button
    type: UpgradeNote, // A FormApplication subclass which should be created
    restricted: false, // Restrict this submenu to gamemaster only?
  });
};
